" line and column number in bottom ruler
set ruler

" line numbers.
set number

" font
set guifont=Cascadia\ Mono:h11:cDEFAULT

" syntax highlighting.
syntax enable

" show matching braces
set showmatch

" programming
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set smartindent
set backspace=indent,eol,start " set backspace function

" folding on indent level
" setting foldlevelstart ensures that for newly opened files folds are open
" unless they are 10 levels deep
set foldmethod=indent
set foldenable
set foldlevelstart=10
set foldnestmax=10

" theme.
color torte

" search highlighting
set hlsearch
set incsearch

" search
set ignorecase
set smartcase

" command line completion
set wildmenu
set wildmode=full

" line wrapping
set wrap
set linebreak

" key mappings
imap jj <ESC>
